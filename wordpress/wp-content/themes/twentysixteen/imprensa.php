<?php
/**
 * Template Name: IMPRENSA PAGE
 */
?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <?php endif; ?>

  <?php wp_head(); ?>
  <meta charset="utf-8">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  

  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  

<link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/nexatlas/css/style.css">
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/nexatlas/css/components/content.css">
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/nexatlas/css/components/button.css">
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/nexatlas/css/pushy.css">
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/nexatlas/css/imprensa.css">
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/nexatlas/css/meteorology.css">
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/nexatlas/css/press.css">


  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!--#4B5155-->
<body id="body2">
                <div class="bar text-right grey">
                    <a href="<?php echo site_url(); ?>">X</a>
                </div>
                <div class="container">
                <div class="row">
                <div class="col-md-12 text-center grey">
                    <h1 class="nexatlas-content-title mont-serrat-semibold-font">NEXATLAS NA MÍDIA</h1>
                </div>
                <div class="col-md-12 text-center grey">
                        NexAtlas está na mídia! Acompanhe as notícias que saem sobre nós.</div>
                </div>
                
                <section class="imprensa-section">
                <div class="row">
                 <?php 
if ( get_query_var( 'paged' ) ) { $paged = get_query_var( 'paged' ); }
elseif ( get_query_var( 'page' ) ) { $paged = get_query_var( 'page' ); }
else { $paged = 1; }
                    query_posts('category_name=imprensa&posts_per_page=4&paged=' . $paged);
                    while (have_posts()) : the_post(); ?>
                    <div class="col-md-6 col-xs-12 my-row">
                        <div class="background-white-layer">
                            <div class="col-md-5 col-xs-12">
                                <?php the_post_thumbnail('large', array('class' => 'img-responsive')); ?>
                            </div>
              
                            <div class="col-md-7 col-xs-12">
                                <div class="title">
                                    <?php the_title(); ?>
                                </div>
                                <div class="text">
                                     <?php the_content(); ?>
                                </div>
                            
                                <div class="clear"></div>
                                <div class="link text-right">
                                    <a href="<?php echo get_field("ext-url");?>" title="<?php the_title(); ?>">Leia matéria completa</a>
                                </div>
                            </div>
                        </div>
                    </div>
                  <?php endwhile; // End the loop. Whew.  ?>
                    </div>              
                    <?php nexatlas_pagination(); ?>
                </section>
                                    <div class="row">
                <div class="col-md-12 text-center grey">
                    <h1 class="nexatlas-content-title mont-serrat-semibold-font">MEDIA TOOLKIT</h1>
                </div>
                <div class="col-md-12 text-center grey">
                         Desenvolvido para auxiliar profissionais de mídia. Aqui você pode encontrar nossas logos, cores, fotos da aplicação e outros materiais</div>
                </div>
                <section class="imprensa-section">


                <div class="row">
                    <div class="col-md-4">
                        <img class="img-responsive center-block" src="<?php echo get_bloginfo('template_url'); ?>/nexatlas/images/modal/imprensa_01.gif" alt="">
                    </div>

                    <div class="col-md-4">
                        <img class="img-responsive center-block" src="<?php echo get_bloginfo('template_url'); ?>/nexatlas/images/modal/imprensa_02.gif" alt="">
                    </div>

                    <div class="col-md-4">
                        <img class="img-responsive center-block" src="<?php echo get_bloginfo('template_url'); ?>/nexatlas/images/modal/imprensa_03.gif" alt="">
                    </div>                                        
                    </div>
                </section>
                </div>
                <!--<div class="footer">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                 <button class="nexatlas-button">CONTATO</button>
                            </div>
                        </div>
                    </div>
                    
                </div>-->

  <!-- Google CDN jQuery with fallback to local -->
  <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>

  <!-- jquery.address plugin CDN -->
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery.address/1.6/jquery.address.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  
  <script src="<?php echo get_bloginfo('template_url'); ?>/nexatlas/js/pushy.js"></script>


  <script src="https://use.fontawesome.com/05fb6d84d9.js"></script>
</body>

</html>
