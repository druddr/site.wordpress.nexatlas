var pageTimer;
var switchTimer;/*jQuery onload*/
var optionSideLeft;
var hash;
var menu;

jQuery(function() {
  //jQuery functions
  jQuery.fn.isOnScreen = function() {
    var element = this.get(0);
    var bounds = element.getBoundingClientRect();
    return bounds.top < window.innerHeight && bounds.bottom > 0
  };/*isOnScreen function*/

  function backgroundVideosIframeLoaded() {
    jQuery('#nexatlas-home').css('background', 'none');
  }

  jQuery('#backgroundVideosId').on('load', backgroundVideosIframeLoaded);

  //Object behaviours implement by the use o jQuery
  jQuery("#navigation-menu a, a[rel='m_PageScroll2id']").mPageScroll2id({
    highlightSelector: "#navigation-menu a",
    clickEvents: !1
  });

  jQuery("#side-navigation a, a[rel='m_PageScroll2id']").mPageScroll2id({
    clickEvents: !1
  });

  jQuery("#navigation-menu a, #side-navigation a, a[rel='m_PageScroll2id']").click(function(e) {

    e.preventDefault();
    /*Comentado*/
   

    var href = jQuery(this).attr("href");
    //console.log(href);
    if(typeof(href) !== "undefined")
    {
    if((href.indexOf('#'))!=-1)
    {
      jQuery.mPageScroll2id("scrollTo", href, {
        clicked: jQuery("a[href='" + href + "']")
      });
    }else{
        window.location.href = href;
    }
    }
  });

  document.querySelectorAll("#side-slide-switch li").forEach(function(e) {
    e.addEventListener("click", function() {
      clearTimeout(switchTimer);
      switchTimer = null;
      switchTimer = createSwitchTimer();
      if (jQuery(this).hasClass('active'))
        return false;
      jQuery("[id*=\"side-description\"]").removeClass("active");
      jQuery("#side-description-" + jQuery(this).children('span').html()).addClass("active");
      jQuery("#side-slide-switch li").removeClass("active before-active");
      jQuery(this).addClass("active").prevAll("li").addClass("before-active")
    })
  })

  optionSideLeft = {
    button: "side-left-pushy-button",
    container: "content",
    containerPush: !1,
    menuPosition: "left",
    menuOpen: !1,
    overlayShow: !0
  };
  jQuery("#side-navigation").Pushy(optionSideLeft)
  pageCheckTimer = createPageCheckTimer()
  window.onhashchange = locationHashChanged;

  //Configures scroll 2 id mousewheel and arrow events
  jQuery(window).on("load", function() {
    configureScroll2Id();
  })
});

function locationHashChanged() {

  hash = window.location.hash.substr(1);

  changeIconActive(hash);

  if (hash == 5) {
    jQuery('#navigation-menu').addClass('menu-nexatlas-branco');
    jQuery('head').append('<style>#navigation-menu .action::before{color: #4b5155;}</style>');
    jQuery('head').append('<style>#navigation-menu .separator::before{color: #4b5155;}</style>')

  } else {
    jQuery('#navigation-menu').removeClass('menu-nexatlas-branco');
    jQuery('head').append('<style>#navigation-menu .action::before{color: #95999a;}</style>');
    jQuery('head').append('<style>#navigation-menu .separator::before{color: #95999a;}</style>')
  }
}

function changeIconActive() {

  menu = window.location.hash.substr(1);

  jQuery('#navigation-menu li').removeClass('menu-icon-verde');
  jQuery('.menu-nexatlas').removeClass('menu-icon-branco');

  if (menu == 1) {
    jQuery('.menu-home').addClass('menu-icon-verde');
    jQuery('.menu-nexatlas').removeClass('menu-icon-branco')
  } else if (menu == 2) {
    jQuery('.menu-cartas').addClass('menu-icon-verde');
    jQuery('.menu-nexatlas').removeClass('menu-icon-branco')
  } else if (menu == 3) {
    jQuery('.menu-rotas').addClass('menu-icon-verde');
    jQuery('.menu-nexatlas').removeClass('menu-icon-branco')
  } else if (menu == 4) {
    jQuery('.menu-meteorologia').addClass('menu-icon-verde');
    jQuery('.menu-nexatlas').removeClass('menu-icon-branco')
  } else if (menu == 5) {
    jQuery('.menu-nexatlas').addClass('menu-icon-branco');
    jQuery('#navigation-menu').addClass('menu-nexatlas-branco');
    jQuery('head').append('<style>#navigation-menu .action::before{color: #4b5155;}</style>');
    jQuery('head').append('<style>#navigation-menu .separator::before{color: #4b5155;}</style>')
  }
}

//performs the page check timer creation
function createPageCheckTimer() {
  return setInterval(function() {
    pageCheck();
  }, 300)
}

//this timer checks which kind of page the buttons layer has to contrast
function pageCheck() {
  //here we hide buttons/elements which should not appear at the main screen
  if (jQuery("#nexatlas-home").isOnScreen()) {
    jQuery(".hide-on-home").hide();
  } else {
    jQuery(".hide-on-home").show();
  }

  //If we are on the charts slide page, checks whether the timeout
  //already exists and create it; else, we have to kill it
  if (jQuery("#nexatlas-cartas").isOnScreen()) {
    if (!switchTimer) {
      switchTimer = createSwitchTimer(0);
    }
  } else {
    clearTimeout(switchTimer);
    switchTimer = null;
  }

  //paqwesd
  if($( window ).width()>900)
  {
  if($( "#nexatlas-press" ).isOnScreen())
  {
    //console.log('cambia el menu');
     jQuery("#navigation-menu").hide();
     jQuery("#navigation-menu2").show();
  }else{
     //console.log('se queda igual');
     jQuery("#navigation-menu").show();
     jQuery("#navigation-menu2").hide();
     
  }
}else{
	 jQuery("#navigation-menu").hide();
     jQuery("#navigation-menu2").hide();
}

  if ($( "#nexatlas-press" ).hasClass( "green-screen" ) && jQuery(".green-screen").isOnScreen()) {
    jQuery("#nexatlas-reusable-buttons-section, #nexatlas-logo").addClass("contrast-with-green");
    jQuery("#nexatlas-reusable-buttons-section, #nexatlas-logo").removeClass("contrast-with-grey contrast-with-mostly-everything");
    
  } else

  if (jQuery(".grey-screen").isOnScreen()) {
    jQuery("#nexatlas-reusable-buttons-section, #nexatlas-logo").addClass("contrast-with-grey");
    jQuery("#nexatlas-reusable-buttons-section, #nexatlas-logo").removeClass("contrast-with-green contrast-with-mostly-everything");
  } else{
    if(!jQuery("#nexatlas-press").isOnScreen())
    {
    jQuery("#nexatlas-reusable-buttons-section, #nexatlas-logo").addClass("contrast-with-mostly-everything");
    jQuery("#nexatlas-reusable-buttons-section, #nexatlas-logo").removeClass("contrast-with-grey contrast-with-green");
    //console.log('aqui no debria entrar');
    }else{
      jQuery("#nexatlas-reusable-buttons-section, #nexatlas-logo").addClass("contrast-with-grey");
    jQuery("#nexatlas-reusable-buttons-section, #nexatlas-logo").removeClass("contrast-with-green contrast-with-mostly-everything");
    }
  }

}

//this timer switches between slides on the specific page #nexatlas-cartas automatically
function createSwitchTimer(pageIndex) {
  //If a page index was specified, selects that slide and hides the others
  //This is used for when the #nexatlas-cartas page is being accessed
  if (pageIndex) {
    if (!jQuery("#side-slide-switch li:eq(" + pageIndex + ")").hasClass("active")) {
      jQuery("#side-slide-switch li:eq(" + pageIndex + ")").addClass("active");
    }
    jQuery("#side-slide-switch li").not(":eq(" + pageIndex + ")").removeClass("active");
  }

  //Then creates the timeout function
  return window.setTimeout(function() {

    //Checks whether the next page should be the first page
    if (jQuery("#side-slide-switch li.active").next().length == 0) {
      jQuery("#side-slide-switch li").first().trigger("click")
    } else {
      jQuery("#side-slide-switch li.active").next().trigger("click")
    }
  }, 3000)
}

//NexAtlas Modal functionalities
/*var modal = document.querySelector("#nexatlas-press-modal");
var modalOverlay = document.querySelector(".nexatlas-page-overlay");
var closeButton = document.querySelector(".close-button");
var openButton = document.querySelector("#press-page-button");

closeButton.addEventListener("click", function() {
  modal.classList.toggle("closed");
  modalOverlay.classList.toggle("closed");
});

openButton.addEventListener("click", function() {
  modal.classList.toggle("closed");
  modalOverlay.classList.toggle("closed");
});

// load svg images to inline svgs
function replaceImgSVG() {
  jQuery('.convert-svg').each(function() {
    var $img = jQuery(this);
    var imgID = $img.attr('id');
    var imgClass = $img.attr('class');
    var imgURL = $img.attr('src');

    jQuery.get(imgURL, function(data) {
      // Get the SVG tag, ignore the rest
      var $svg = jQuery(data).find('svg');

      // Add replaced image's ID to the new SVG
      if (typeof imgID !== 'undefined') {
        $svg = $svg.attr('id', imgID);
      }
      // Add replaced image's classes to the new SVG
      if (typeof imgClass !== 'undefined') {
        $svg = $svg.attr('class', imgClass + ' replaced-svg');
      }

      // Remove any invalid XML tags as per http://validator.w3.org
      $svg = $svg.removeAttr('xmlns:a');

      // Replace image with new SVG
      $img.replaceWith($svg);
    }, 'xml');

  });
}

jQuery(document).ready(function() {
  replaceImgSVG();
});

/* Function taken from Scroll 2 ID website: http://manos.malihu.gr/page-scroll-to-id-with-mousewheel-and-keyboard/ */
function configureScroll2Id() {

  if (!jQuery(document).data("mPS2id")) {
    console.log("Error: 'Page scroll to id' plugin not present or activated. Please run the code after plugin is loaded.");
    return;
  }

  jQuery(document).data("mPS2idExtend", {
    selector: "._mPS2id-h",
    currentSelector: function() {
      return this.index(jQuery(".mPS2id-highlight-first").length
        ? jQuery(".mPS2id-highlight-first")
        : jQuery(".mPS2id-highlight"));
    },
    input: {
      y: null,
      x: null
    },
    i: null,
    time: null
  }).on("scrollSection", function(e, dlt, i) {
    var d = jQuery(this).data("mPS2idExtend"),
      sel = jQuery(d.selector);
    if (!jQuery("html,body").is(":animated")) {
      if (!i)
        i = d.currentSelector.call(sel);
      if (!(i === 0 && dlt > 0) && !(i === sel.length - 1 && dlt < 0))
        sel.eq(i - dlt).trigger("click.mPS2id");
      }
    }).on("mousewheel DOMMouseScroll", function(e) { //mousewheel
    if (jQuery(jQuery(this).data("mPS2idExtend").selector).length)
      e.preventDefault();
    jQuery(this).trigger("scrollSection", ((e.originalEvent.detail < 0 || e.originalEvent.wheelDelta > 0)
      ? 1
      : -1));
  }).on("keydown", function(e) { //keyboard
    var code = e.keyCode
        ? e.keyCode
        : e.which,
      keys = jQuery(this).data("mPS2id").layout === "horizontal"
        ? [37, 39]
        : [38, 40];
    if (code === keys[0] || code === keys[1]) {
      if (jQuery(jQuery(this).data("mPS2idExtend").selector).length)
        e.preventDefault();
      jQuery(this).trigger("scrollSection", (code === keys[0]
        ? 1
        : -1));
    }
  }).on("pointerdown touchstart", function(e) { //touch (optional)
    var o = e.originalEvent,
      d = $(this).data("mPS2idExtend");
    if (o.pointerType === "touch" || e.type === "touchstart") {
      var y = o.screenY || o.changedTouches[0].screenY;
      d.input.y = y;
      if ($(this).data("mPS2id").layout === "horizontal") {
        var x = o.screenX || o.changedTouches[0].screenX;
        d.input.x = x;
      }
      d.time = o.timeStamp;
      d.i = d.currentSelector.call($(d.selector));
    }
  }).on("touchmove", function(e) {
    if ($("html,body").is(":animated"))
      e.preventDefault();
    }
  ).on("pointerup touchend", function(e) {
    var o = e.originalEvent;
    if (o.pointerType === "touch" || e.type === "touchend") {
      var y = o.screenY || o.changedTouches[0].screenY,
        d = $(this).data("mPS2idExtend"),
        diff = d.input.y - y,
        time = o.timeStamp - d.time,
        i = d.currentSelector.call($(d.selector));
      if ($(this).data("mPS2id").layout === "horizontal") {
        var x = o.screenX || o.changedTouches[0].screenX,
          diff = d.input.x - x;
      }
      if (Math.abs(diff) < 2)
        return;
      var _switch = function() {
        return time < 200 && i === d.i;
      };
      $(this).trigger("scrollSection", [
        (diff > 0 && _switch()
          ? -1
          : diff < 0 && _switch()
            ? 1
            : 0),
        (_switch()
          ? d.i
          : i)
      ]);
    }
  });
}

////Commented cause it's the first thing to be done
//jQuery(window).on('load', function() {
//checkLoggedInUser();
//})

// Checks if there are these values on localstorage:
// SCRIPT MOVED TO WORDPRESS HEADER
/*
  #current$user@        - needs to have an ID
  #current$userProfile@ - needs to have an ID
  #remember$user@       - needs to be true
*/
/*
function checkLoggedInUser() {
  if (window.localStorage) {
    var currentUser = JSON.parse(localStorage.getItem("#current$user@"));
    var currentUserProfile = JSON.parse(localStorage.getItem("#current$userProfile@"));
    var rememberUser = JSON.parse(localStorage.getItem("#remember$user@"));
    if (currentUser && currentUser.id && currentUserProfile.id && rememberUser) {
      // TODO: define redirect URL
      var redirectURL = window.location.protocol + "//" + window.location.hostname + "/app/#/app/dashboard/i";
      console.info("Redirecting to: " + redirectURL);
      window.location.assign(redirectURL);
    }
  } else {
    // not possible to acccess the local storage
    console.error("Local Storage not available");
  }
}
*/