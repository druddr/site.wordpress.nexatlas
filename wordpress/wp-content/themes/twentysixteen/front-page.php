<?php
/**
 * Template Name: Frontpage
 */
?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <?php endif; ?>

  <?php wp_head(); ?>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  

  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  

<link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/nexatlas/css/style.css">
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/nexatlas/css/components/content.css">
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/nexatlas/css/components/button.css">
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/nexatlas/css/pushy.css">
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/nexatlas/css/nexatlas-general.local.css">
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/nexatlas/css/meteorology.css">
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/nexatlas/css/press.css">


  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!--#4B5155-->
<body id="body">
  <!-- Start: Navigation Menu -->
  <!-- Start: Small hamburger button -->
  <div id="small-hamburger-menu" class="small-hamburger-menu side-left-pushy-button">
   <img class="convert-svg" alt="menu" src="<?php echo get_bloginfo('template_url'); ?>/nexatlas/images/general/hamburger_icon-01.svg"  />
  </div>
  <!-- End: Small hamburger button -->

  <!-- Start: main navigation menu (timeline) -->
  <nav id="navigation-menu" class="navigation">
    <ul id="navigation-menu-menu" class="menu">

      <li class="separator"></li>

      <li class="action menu-home"><a href="#nexatlas-home" rel="m_PageScroll2id">INÍCIO</a></li>

      <li class="separator"></li>

      <li class="action menu-cartas"><a href="#nexatlas-cartas" rel="m_PageScroll2id">CARTAS</a></li>

      <li class="separator"></li>

      <li class="action menu-rotas"><a href="#nexatlas-rotas" rel="m_PageScroll2id">ROTAS</a></li>

      <li class="separator"></li>

      <li class="action menu-meteorologia"><a href="#nexatlas-meteorologia" rel="m_PageScroll2id">METEOROLOGIA</a></li>

      <li class="separator"></li>

      <li class="action menu-nexatlas"><a href="#nexatlas-press" rel="m_PageScroll2id">NEXATLAS</a></li>

      <li class="separator"></li>
    </ul>
  </nav>



  <nav id="navigation-menu2" class="navigation">
    <ul id="navigation-menu-menu2" class="menu">

      <li class="separator"></li>

      <li class="action menu-home"><a href="#nexatlas-home" rel="m_PageScroll2id">INÍCIO</a></li>

      <li class="separator"></li>

      <li class="action menu-cartas"><a href="#nexatlas-cartas" rel="m_PageScroll2id">CARTAS</a></li>

      <li class="separator"></li>

      <li class="action menu-rotas"><a href="#nexatlas-rotas" rel="m_PageScroll2id">ROTAS</a></li>

      <li class="separator"></li>

      <li class="action menu-meteorologia"><a href="#nexatlas-meteorologia" rel="m_PageScroll2id">METEOROLOGIA</a></li>

      <li class="separator"></li>

      <li class="action menu-nexatlas"><a href="#nexatlas-press" rel="m_PageScroll2id">NEXATLAS</a></li>

      <li class="separator"></li>
    </ul>
  </nav>



  <!-- End: main navigation menu (timeline) -->

  <!-- Start: side navigation menu (drawer) -->
  <nav id="side-navigation" class="pushy pushy-left">

    <a id="side-navigation-close-btn" class="closebtn side-left-pushy-button">
      <img class="convert-svg" alt="close" src="<?php echo get_bloginfo('template_url'); ?>/nexatlas/images/general/close_button-01.svg"  />
    </a>

    <ul id="side-navigation-menu-sidenav" class="menu-sidenav">
      <li class=""><a class="side-left-pushy-button" href="#nexatlas-home">INÍCIO</a> </li>
      <li class=""><a class="side-left-pushy-button" href="#nexatlas-cartas">CARTAS</a></li>
      <li class=""><a class="side-left-pushy-button" href="#nexatlas-rotas">ROTAS</a> </li>
      <li class=""><a class="side-left-pushy-button" href="#nexatlas-meteorologia">METEOROLOGIA</a> </li>
      <li class=""><a class="side-left-pushy-button" href="#nexatlas-press">NEXATLAS</a></li>
      <li class=""><a class="side-left-pushy-button" href="<?php echo site_url()?>/app">LOGIN</a></li>
      <li class=""><a class="side-left-pushy-button" href="<?php echo site_url()?>/imprensa"><div class="grey2">NEXATLAS NA MÍDIA</div></a></li>
      <li class=""><a class="side-left-pushy-button" href="#"><div class="grey2">TERMOS DE USO</div></a></li>
    </ul>

	<ul id="side-navigation-menu-sidenav" class="menu-sidenav2">

    </ul>

    <ul id="side-navigation-sidenav-social-icons" class="sidenav-social-icons">

      <li>
        <a href="#" target="_blank">
          <i class="fa fa-instagram" aria-hidden="true"></i>
        </a>
      </li>

      <li>
        <a href="#" target="_blank">
          <i class="fa fa-facebook" aria-hidden="true"></i>
        </a>
      </li>

      <li>
        <a href="#" target="_blank">
          <i class="fa fa-linkedin" aria-hidden="true"></i>
        </a>
      </li>

    </ul>

  </nav>
  <!-- End: side navigation menu (drawer) -->

  <!-- End: Navigation Menu -->
  <div id="content">

    <div id="nexatlas-logo">
    	<span id="logo-contrast-with-mostly-everything">
     		<img src="<?php echo get_bloginfo('template_url'); ?>/nexatlas/images/general/logo_negativo.png" width="204" height="45" alt="NexAtlas" >
      	</span>
      	<span id="logo-contrast-with-grey">
        	<img src="<?php echo get_bloginfo('template_url'); ?>/nexatlas/images/general/logo_positivo-01.png" width="204" height="45" alt="NexAtlas" >
      	</span>
      	<span id="logo-contrast-with-green">
        	<img src="<?php echo get_bloginfo('template_url'); ?>/nexatlas/images/general/logo_branco.png" width="204" height="45" alt="NexAtlas">
      	</span>
    </div>

    <div id="nexatlas-reusable-buttons-section" class="fixed-element contrast-with-mostly-everything">
      <div class="nexatlas-login-button-section">
        <div class="link-button-list hide-on-home">
          <a id="nexatlas-login-button" class="link-button" href="app/">Login</a>
        </div>
        <div class="fusion-clearfix"></div>
      </div>
      <div class="nexatlas-contact-button-section">
        <button class="nexatlas-button nexatlas-contact-button hide-on-home">Contato</button>
        <div class="fusion-clearfix">
        </div>
      </div>
    </div>
	<!-- Start: nexatlas-home -->
    <section class="scrollify" id="nexatlas-home">
        <div class="fullscreen-bg">
            <video  poster="<?php echo get_field('fallback'); ?>" autoplay="autoplay" loop="loop" class="fullscreen-bg__video" preload="auto" volume="50"/>
                <source src="<?php echo get_field('video_mp4'); ?>" type="video/mp4" />
                <source src="<?php echo get_field('video_ogv'); ?>" type="video/ogg" />
                <source src="<?php echo get_field('video_webm'); ?>" type="video/webm" />
            </video>
        </div>
        <div class="center-layer">
            <h1 class="nexatlas-content-title centered mont-serrat-regular-font">
            	SEU PLANEJAMENTO DE VOO<br>MAIS PROFISSIONAL
            </h1>
        	<p class="new-p nexatlas-content-text mont-serrat-light-font"><?php 
				$id=4; 
				$post = get_post($id); 
				$content = apply_filters('the_content', $post->post_content); 
				echo $content;  
			?></p>
        	<p class="nexatlas-content-text">
          		<a href="app/" class="nexatlas-button mont-serrat-light-font nexatlas-call-to-action nexatlas-button-margin-top white-text">Acesse agora</a>
        	</p>
        	<div class="nexatlas-scroll-down-tip block-center">
        		<a href="#nexatlas-cartas" rel="m_PageScroll2id" class="_mPS2id-h">
            		<img src="<?php echo get_bloginfo('template_url'); ?>/nexatlas/images/general/scroll-down-tip-40-width.png" width="40" height="69" alt="down">
          		</a>
        	</div>
       	</div>

    </section>
	<!-- Start: nexatlas-cartas -->
    <section class="page scrollify" id="nexatlas-cartas">
        <div class="container">
            <div class="row">

                <div class="col-md-offset-1 col-md-6 col-lg-offset-1 col-lg-6 col-xs-12">
                    <object alt="CARTAS" class="text-top-icon" type="image/svg+xml" data="<?php echo get_bloginfo('template_url'); ?>/nexatlas/images/general/ico_mapa.svg">
                     Your browser does not support SVGs
                    </object>
                    <h2 class="nexatlas-content-title mont-serrat-semibold-font">VISUALIZE CARTAS E MAPAS INTERATIVOS</h2>
                    <div class="new-p mont-serrat-light-font color-font text-left">
                        <?php 
                        $id=6; 
                        $post = get_post($id); 
                        $content = apply_filters('the_content', $post->post_content); 
                        echo $content;  
                        ?>
                    </div>
                </div>
                <div class="col-lg-4  col-md-3 hidden-at-800">
                    <div class="nexatlas-content-right-column  hidden-at-800">
                      <ul id="side-slide-switch" class="hidden-list">
                        <li class="active"><span>01</span></li>
                        <li><span>02</span></li>
                        <li><span>03</span></li>
                        <li><span>04</span></li>
                        <li><span>05</span></li>
                      </ul>
                      <div id="side-slide-description">
                        <div id="side-description-01" class="active">
                          <h2>WAC</h2>
                          <p>Carta Aeronáutica<br>Mundial</p>
                        </div>
                        <div id="side-description-02">
                          <h2>REA</h2>
                          <p>Rotas Especiais<br>de Aeronaves</p>
                        </div>
                        <div id="side-description-03">
                          <h2>REH</h2>
                          <p>Rotas Especiais<br>de Helicópteros</p>
                        </div>
                        <div id="side-description-04">
                          <h2>ENRCL</h2>
                          <p>Cartas de Rotas do<br>Espaço Aéreo Inferior</p>
                        </div>
                        <div id="side-description-05">
                          <h2>ENRCH</h2>
                          <p>Cartas de Rotas do<br>Espaço Aéreo Superior</p>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
	<!-- Start: nexatlas-rotas -->
    <section class="page grey-screen scrollify" id="nexatlas-rotas">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-1 col-lg-offset-1 col-lg-6 col-xs-12">
                    <object alt="ROTAS" class="text-top-icon" type="image/svg+xml" data="<?php echo get_bloginfo('template_url'); ?>/nexatlas/images/general/ico_rotas.svg">
                    Your browser does not support SVGs
                    </object>
                    <h2 class="nexatlas-content-title mont-serrat-semibold-font">ESCOLHA A ROTA PARA O SEU VOO</h2>
                    <div class="new-p grey2 mont-serrat-light-font color-font text-left">
                         <?php 
                        $id=8; 
                        $post = get_post($id); 
                        $content = apply_filters('the_content', $post->post_content); 
                        echo $content;  
                        ?> 
                    </div>
                </div>
                <div class="col-lg-4  col-md-3 hidden-at-800">
                          <object alt="ROTAS" type="image/svg+xml" data="<?php echo get_bloginfo('template_url'); ?>/nexatlas/images/general/vetor_animacao_2-01.svg">
                        Your browser does not support SVGs
                    </object>
                </div>

            </div>
        </div>
    </section>

    <!-- Start: nexatlas-meteorologia -->
    <section class="page scrollify" id="nexatlas-meteorologia">
        <div class="container">
            <div class="row">
                <div class="col-lg-offset-1 col-md-offset-1 col-lg-8 col-md-11 col-xs-12">
                    <object alt="Metereologia" class="text-top-icon" type="image/svg+xml" data="<?php echo get_bloginfo('template_url'); ?>/nexatlas/images/general/ico_meteorologia.svg">Your browser does not support SVGs</object>
                    <h2 class="nexatlas-content-title mont-serrat-semibold-font">
                        INFORMAÇÕES PRECISAS PARA A SUA TOMADA DE DECISÃO</h2>
                    <div class="new-p mont-serrat-light-font color-font text-left">
                        <?php 
                            $id=10; 
                            $post = get_post($id); 
                            $content = apply_filters('the_content', $post->post_content); 
                            echo $content;  
                        ?>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- End: nexatlas-meteorologia -->

    <!-- Start: nexatlas-press -->
    <section class="page green-screen scrollify" id="nexatlas-press">
        <div class="container">
            <div class="row hidden-xs">
                <div class="col-md-12 text-center">
                        <img src="<?php echo get_bloginfo('template_url'); ?>/nexatlas/images/general/NEX_07__nexatlas_imagem_03.png" alt="Nexatlas" title="NEX_07__nexatlas_imagem_03" alt="nexatlas-img" class="img-nexatlas center-block"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 text1 col-xs-12">
                    Modernizando a arte de voar
                </div>
                <div class="col-md-8 text2">
                    <div class="new-p mont-serrat-light-font white-text text-left">
                        <span class="text2">
                            <?php 
                            $id=12; 
                            $post = get_post($id); 
                            $content = apply_filters('the_content', $post->post_content); 
                            echo $content;  
                            ?>
                        </span> 
                    </div>
                </div>
            </div>
            <div class="row logos">
                <div class="col-md-5 col-md-offset-3 ">
                    <div class="col-xs-4 text-Left text2">
                        <span class="white-text">Accelerated by</span>
                    </div>
                    <div class="col-xs-4">
                        <img class="img-responsive center-block" id="seed" src="<?php echo get_bloginfo('template_url'); ?>/nexatlas/images/general/logo_seed.png" alt="Seed">
                    </div>
                    <div class="col-xs-4">
                        <img class="img-responsive" id="start-up-chile" src="<?php echo get_bloginfo('template_url'); ?>/nexatlas/images/general/logo_startupchile.png" alt="Start-Up Chile" data-pin-nopin="true">
                    </div>
                </div>
            </div>
            <div class="row">
              <div class="col-md-5 col-md-offset-3 text-right">
                          <div class="col-xs-12 text-center  mont-serrat-light-font white-text ">
                <a  class="imprensa" href="/imprensa">IMPRENSA</a> | <span class="imprensa">CONTATO@NEXATLAS.COM </span>
                </div>
            </div>
            </div>

        </div>
                



            <div class="clear"></div>

  </section>
  <!-- End: nexatlas-press -->

  

  <!-- Google CDN jQuery with fallback to local -->
  <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>

  <!-- jquery.address plugin CDN -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.address/1.6/jquery.address.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
    
  <script src="<?php echo get_bloginfo('template_url'); ?>/nexatlas/js/scrollify.js"></script>
  <script src="<?php echo get_bloginfo('template_url'); ?>/nexatlas/js/pushy.js"></script>
  <script src="<?php echo get_bloginfo('template_url'); ?>/nexatlas/js/m_PageScroll2id.js"></script>
  <script src="<?php echo get_bloginfo('template_url'); ?>/nexatlas/js/nexatlas-general.local.js"></script>
  

        <script>
         
        $(function() {

          $.scrollify({
            section : ".scrollify",
          easing: "easeOutExpo",
          scrollSpeed: 500,
          offset : 0,
          scrollbars: true,
          standardScrollElements: "",
          setHeights: true,
          overflowScroll: false,
          updateHash: false,
          touchScroll:true
          });

        });
      </script>

  <script src="https://use.fontawesome.com/05fb6d84d9.js"></script>
</body>

</html>
